<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <style>
    .flex-container {
  display: flex;
  flex-wrap: nowrap;
  
}
.flex-container > div {
  width:240px;
  margin:10px;
  text-align: center;
  line-height:90px;
  font-size:110px;
  max-width: 100%;
}
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home - IUGALE Constructions</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.ico">
    <!-- CSS
		============================================ -->
    <?php
        require_once('menu.php');
    ?>
    <!--====================  hero slider area ====================-->
    <div class="hero-area space__bottom--r120">
        <div class="hero-area-wrapper">
            <div class="single-hero-slider single-hero-slider--background position-relative bg-img" data-bg="assets/img/hero-slider/home2-slider1-image2.jpg">
                <!-- hero slider content -->
                <div class="single-hero-slider__abs-img">
                    <img src="assets/img/hero-slider/home2-slider1.png" class="img-fluid" alt="">
                    <img src="assets/img/hero-slider/home2-slider2.png" class="img-fluid" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hero-slider-content hero-slider-content--extra-space">
                                <h3 class="hero-slider-content__subtitle hero-slider-content__subtitle hero-slider-content__subtitle--dark">IUGALE Constructions</h3>
                                <h2 class="hero-slider-content__title hero-slider-content__title--dark space__bottom--50">We Construct your Dream in Live</h2>
                                <a href="contact" class="default-btn default-btn--hero-slider">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of hero slider area  ====================-->
    <!--====================  feature area ====================-->
    <div class="feature-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-6 space__bottom__md--40 space__bottom__lm--40">
                    <!-- feature content image -->
                    <div class="feature-content-image">
                        <img src="assets/img/feature/feature-banner-3.jpg" class="img-fluid" alt="">
                        <img src="assets/img/feature/feature-banner-2.jpg" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-xl-5 offset-xl-1">
                    <!-- feature content wrapper -->
                    <div class="feature-content-wrapper space__bottom--m40">
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img src="assets/img/icons/feature-1.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Top Rated</h4>
                                <p class="single-feature__text">Have finished in excess of 35 undertakings with best client support and fulfillment.</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img src="assets/img/icons/feature-2.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Best Quality</h4>
                                <p class="single-feature__text">Quality guaranteed as a most extreme need in the entirety of our undertakings with standard material utilized.</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img src="assets/img/icons/feature-3.png" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Low Price</h4>
                                <p class="single-feature__text">Additionally, having all endeavors fitting the spending limit with most minimal cost in showcase.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of feature area  ====================-->
    <!--====================  about area ====================-->
    <div class="about-area space__bottom--r120 ">
        <div class="container">
            <div class="row align-items-center row-25">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="about-content">
                        <!-- section title -->
                        <div class="section-title space__bottom--25">
                            <h3 class="section-title__sub">Since 2010</h3>
                            <h2 class="section-title__title">Provide the best quality service and construct</h2>
                        </div>
                        <p class="about-content__text space__bottom--40" align="justify">Group of 3 that has full filled the dreams with their headway adventures in building since 2010.Looking forward to make your dream projects real.</p>
                        <a href="contact" class="default-btn">Start now</a>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-2">
                    <div class="about-image space__bottom__lm--30">
                        <img src="assets/img/about/about-section-2.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of about area  ====================-->
    <!--====================  service area ====================-->
    <div class="service-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-5">
                    <!-- service banner -->
                    <div class="service-banner space__bottom__md--40 space__bottom__lm--40">
                        <img src="assets/img/service/service-man-2.png" class="float-none float-lg-left" alt="service man">
                    </div>
                </div>
                <div class="col-xl-9 col-lg-7 space__top--65 space__top__md--0 space__top__lm--0">
                    <!-- section title -->
                    <div class="section-title space__bottom--40">
                        <h3 class="section-title__sub">Our Services</h3>
                        <h2 class="section-title__title" >Unique and Quality Service Makes Client Happy</h2>
                    </div>
                    <!-- service slider -->
                    <div class="row service-slider-wrapper space__bottom__md--40 space__bottom__lm--40">
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-5.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">Construction</a>
                            </h4>
                        </div>
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-4.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">Architecture</a>
                            </h4>
                        </div>
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-6.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">Renovation</a>
                            </h4>
                        </div>
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-1.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">Project Planning</a>
                            </h4>
                        </div>
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-2.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">Labor providing</a>
                            </h4>
                        </div>
                        <div class="col single-service text-center">
                            <div class="single-service__image space__bottom--15">
                                <a href="service.html"><img src="assets/img/service/service-3.jpg" class="img-fluid" alt=""></a>
                            </div>
                            <h4 class="single-service__content">
                                <a href="service.html">On demand services</a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of service area  ====================-->
    <!--====================  cta area ====================-->
    <div class="cta-area cta-area-bg bg-img" data-bg="assets/img/backgrounds/cta-bg2.jpg">
        <div class="cta-wrapper background-color--dark-overlay space__inner__top--50 space__inner__bottom--150">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto">
                        <div class="cta-block cta-block--default-color">
                            <p class="cta-block__light-text text-center">More than <span>30</span>Projects</p>
                            <p class="cta-block__semi-bold-text cta-block__semi-bold-text--medium text-center">Do you have dream projects? Contact us</p>
                            <p class="cta-block__bold-text text-center"> <a href="tel:9380737109">9380737109</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of cta area  ====================-->
    <!-- funfact include -->
    <div class="funfact-wrapper space__top--m100">
        <!--====================  fun fact area ====================-->
        <div class="fun-fact-area space__bottom--r120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- fun fact wrapper -->
                        <div class="fun-fact-wrapper fun-fact-wrapper-bg bg-img" data-bg="assets/img/backgrounds/funfact-bg.jpg">
                            <div class="fun-fact-inner background-color--default-overlay background-repeat--x-bottom space__inner--y30 bg-img" data-bg="assets/img/icons/ruler-black.png">
                                <div class="fun-fact-content-wrapper">
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">32</h3>
                                        <h4 class="single-fun-fact__text">Projects</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">180</h3>
                                        <h4 class="single-fun-fact__text"> Happy Clients</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">17</h3>
                                        <h4 class="single-fun-fact__text">Team</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">15</h3>
                                        <h4 class="single-fun-fact__text">Years</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of fun fact area  ====================-->
    </div>
    <!--====================  project area ====================-->
    <div class="project-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Our Projects</h3>
                        <h2 class="section-title__title">Here you find our latest projects that we did and doing</h2>
                    </div>
                </div>
            </div>
            <div class="project-wrapper project-wrapper--masonry row space__bottom--m30">
                <div class="col-1 gutter"></div>
                <div class="col-lg-4 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0">
                            <img src="assets/img/projects/project9.jpeg" class="img-fluid" alt="">
                            <span class="single-project-title">Apartment</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0">
                            <img src="assets/img/projects/project10.jpeg" class="img-fluid" alt="">
                            <span class="single-project-title">Residence</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0">
                            <img src="assets/img/projects/project11.jpeg" class="img-fluid" alt="">
                            <span class="single-project-title">Enclave</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0">
                            <img src="assets/img/projects/project12.jpeg" class="img-fluid" alt="">
                            <span class="single-project-title">Residence-1</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0">
                            <img src="assets/img/projects/project13.jpeg" class="img-fluid" alt="">
                            <span class="single-project-title">Apartment-1</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project area  ====================-->
    <!--====================  team area ====================-->
    <div class="team-area space__bottom--r120 position-relative">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 space__bottom__md--40 space__bottom__lm--40">
                    <div class="team-member-title-wrapper">
                        <!-- section title -->
                        <div class="section-title space__bottom--30 space__bottom__md--30  space__bottom__lm--20">
                            <h3 class="section-title__sub">Our Team</h3>
                            <h2 class="section-title__title">Best & quality team member</h2>
                        </div>
                        <p class="team-text space__bottom--40 space__bottom__md--30 space__bottom__lm--20" align="justify" hspace="02">Team that is excited and quick to grandstand the class of the work in their Ventures. Join our hands. Let’s chase dreams.</p>
                    </div>
                    </div>
                       <div class="flex-container">
                            <div>
                                <img src="assets/img/team/azeez.jpg" class="img-fluid" alt="" hspace="05">
                                <div style="margin-left:10px">
                                  <h4> Azeez </h4>
                                  <h6>(Co-founder)</h6>
                                  </a>
                                </div>
                            </div>
                            <div>
                                <img src="assets/img/team/haneef.jpg" class="img-fluid" alt="" hspace="15">
                                <div style="margin-left:80px">
                                  <h4> Hanif </h4>
                                  <h6>(Co-founder)</h6>
                                  </a>
                                </div>
                            </div>
                            <div>
                                 <img src="assets/img/team/waseem.png" class="img-fluid" alt="" hspace="10">
                                <div style="margin-left:30px">
                                  <h5> Waseem </h5>
                                  <h6>(Co-founder)</h6>
                                  </a>
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of team area  ====================-->
    <!--====================  testimonial area ====================-->
    <div class="testimonial-area space__bottom--r20">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Testimonials</h3>
                        <h2 class="section-title__title">What client’s say</h2>
                    </div>
                    <!-- testimonial slider -->
                    <div class="testimonial-multi-slider-wrapper space__inner__bottom--50 space__inner__bottom__md--50 space__inner__bottom__lm--50">
                        <div class="single-testimonial single-testimonial--style2">
                            <p align= "justify" class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>Iugale Construction has been associating with us for more than 8 years by fulfilling all our construction requirements with in the time period without compromising the quality.<span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Husna Siddiqua</h5>
                            <p class="single-testimonial__author-des">Software Engineer (Wipro)</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p align= "justify" class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>This apartment is fantastic and we are happy to be staying here. We certainly look forward to our future with no worries about quality and style. We are happy to be a part of Iugale construction. <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Makdoom Sharief</h5>
                            <p class="single-testimonial__author-des">Product Manager (HP)</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p align= "justify" class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>Azeez has always been an active part of our business, by taking up our projects and We always feel thankful for giving the best architecture with in the time frame. <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Syed Tabriaz</h5>
                            <p class="single-testimonial__author-des">Self Employed</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p align= "justify" class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>The team is excellent and they have done an amazing job. Really happy with the service and the timeline they followed. Cost-effective and best quality. Everything at one place. Much appreciated.<span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Deepak K</h5>
                            <p class="single-testimonial__author-des">Self employed</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br>
    <!--====================  End of testimonial area  ====================-->
    <!--====================  blog grid slider area ====================->
    <div class="blog-grid-slider-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title ->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Latest Post</h3>
                        <h2 class="section-title__title">Blog post about our various construction projects</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-grid-wrapper space__bottom--m40">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="single-blog-grid space__bottom--40">
                                    <div class="single-blog-grid__image space__bottom--15">
                                        <a href="blog-details-left-sidebar.html">
                                            <img src="assets/img/blog/grid1.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <h4 class="single-blog-grid__title space__bottom--10"><a href="blog-details-left-sidebar.html"> New design concept & idea</a></h4>
                                    <p class="single-blog-grid__text">Publishing packages and web page editors now use Lorem Ipsum as their default model text and a search for lorem ipsumwill</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="single-blog-grid space__bottom--40">
                                    <div class="single-blog-grid__image space__bottom--15">
                                        <a href="blog-details-left-sidebar.html">
                                            <img src="assets/img/blog/grid2.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <h4 class="single-blog-grid__title space__bottom--10"><a href="blog-details-left-sidebar.html"> Bigest construction design</a></h4>
                                    <p class="single-blog-grid__text">Publishing packages and web page editors now use Lorem Ipsum as their default model text and a search for lorem ipsumwill</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="single-blog-grid space__bottom--40">
                                    <div class="single-blog-grid__image space__bottom--15">
                                        <a href="blog-details-left-sidebar.html">
                                            <img src="assets/img/blog/grid3.jpg" class="img-fluid" alt="">
                                        </a>
                                    </div>
                                    <h4 class="single-blog-grid__title space__bottom--10"><a href="blog-details-left-sidebar.html"> Steel structure design concept</a></h4>
                                    <p class="single-blog-grid__text">Publishing packages and web page editors now use Lorem Ipsum as their default model text and a search for lorem ipsumwill</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of blog grid slider area  ====================-->
    <!--====================  brand logo area ====================->
    <div class="brand-logo-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- brand logo slider ->
                    <div class="brand-logo-wrapper">
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo1.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo2.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo3.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo4.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo5.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img src="assets/img/brand-logo/logo4.png" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!--====================  End of brand logo area  ====================-->
    <!--====================  newsletter area ====================-->
    <div class="newsletter-area newsletter-area-bg bg-img" data-bg="assets/img/backgrounds/newsletter-bg.jpg">
        <div class="newsletter-wrapper background-color--default-overlay space__inner--y60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-10 mx-auto">
                        <div class="row align-items-center">
                            <div class="col-lg-4 mb-3 mb-lg-0">
                                <!-- newsletter title -->
                                <h3 class="newsletter-title"><span>Newsletter</span> Subscribe now</h3>
                            </div>
                            <div class="col-lg-8">
                                <div class="newsletter-form">
                                    <form id="mc-form" class="mc-form">
                                        <input type="email" placeholder="To get update, enter your email">
                                        <button class="theme-button" type="submit">Subscribe</button>
                                    </form>
                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of newsletter area  ====================-->
    <!--====================  footer area ====================-->
     <?php
        require_once('footer.php');
    ?>
