<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
<style>
 div {
  text-align: justify;
  text-justify: inter-word;
}
.flex-container {
  display: flex;
  flex-wrap: nowrap;
  
}

.flex-container > div {
  width:240px;
  margin:10px;
  text-align: center;
  line-height:90px;
  font-size:110px;
  max-width: 100%;
}
</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Term-Condition - IUGALE Constructions</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.ico">
    <!-- CSS
		============================================ -->
    <?php
        require_once('menu.php');
    ?>
    <!--====================  breadcrumb area ====================-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>Term Condition</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="index.html">Home</a></li>
                            <li>Term Condition</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of breadcrumb area  ====================-->
    <!--====================  about area ====================-->
    <div class="about-area space__bottom--r120 ">
        <div class="container">
            <div class="row align-items-center row-25">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="about-content">
                        <!-- section title -->
                        
                        <h4>Term Condition</h4>
                        <p align="justify" style="width:1150px">Welcome to iugaleconstructions.com!
                        These terms and conditions outline the rules and regulations for the use of Iugale Constructions's Website, located at http://iugaleconstructions.com/.
                        By accessing this website we assume you accept these terms and conditions. Do not continue to use iugaleconstructions.com if you do not agree to take all of the terms and conditions stated on this page. Our Terms and Conditions were created with the help of the Terms And Conditions Generator and the Free Terms & Conditions Generator.
                        The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.
                        </p>
                        
                        <h4>Cookies</h4>
                        <p align="justify" style="width:1150px">We employ the use of cookies. By accessing iugaleconstructions.com, you agreed to use cookies in agreement with the Iugale Constructions's Privacy Policy.
                        Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.

                        </p>
                       
                        <h4>License</h4>
                        <p align="justify" style="width:1150px">Unless otherwise stated, Iugale Constructions and/or its licensors own the intellectual property rights for all material on iugaleconstructions.com. All intellectual property rights are reserved. You may access this from iugaleconstructions.com for your own personal use subjected to restrictions set in these terms and conditions.
                       You must not:
                     •   Republish material from iugaleconstructions.com
                     •   Sell, rent or sub-license material from iugaleconstructions.com
                     •   Reproduce, duplicate or copy material from iugaleconstructions.com
                     •   Redistribute content from iugaleconstructions.com
                     This Agreement shall begin on the date hereof.
                    Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Iugale Constructions does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of Iugale Constructions,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Iugale Constructions shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.
                    Iugale Constructions reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.
                    You warrant and represent that:
                    •   You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;
                   •   The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;
                   •   The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy
                   •   The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.
                   You hereby grant Iugale Constructions a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media. </p>
                  
                    <h4>Hyperlinking to our Content</h4>
                    <p align="justify" style="width:1150px">The following organizations may link to our Website without prior written approval:
                        •   Government agencies;
                        •   Search engines;
                        •   News organizations;
                        •   Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and
                        •   System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.
                        These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site. 
                        We may consider and approve other link requests from the following types of organizations:
                        •   commonly-known consumer and/or business information sources;
                        •   dot.com community sites;
                        •   associations or other groups representing charities;
                        •   online directory distributors;
                        •   internet portals;
                        •   accounting, law and consulting firms; and
                        •   educational institutions and trade associations.
                        We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Iugale Constructions; and (d) the link is in the context of general resource information.
                        These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.
                        If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Iugale Constructions. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.
                        Approved organizations may hyperlink to our Website as follows:
                        •   By use of our corporate name; or
                        •   By use of the uniform resource locator being linked to; or
                        •   By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.
                        No use of Iugale Constructions's logo or other artwork will be allowed for linking absent a trademark license agreement.</p>
                         
                        <h4>iFrames</h4>
                        <p align="justify" style="width:1150px">Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.
                        </p>
                        
                        <h4>Content Liability</h4>
                        <p align="justify" style="width:1150px">We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear.on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.
                        </p>
                        
                        <h4>Your Privacy</h4>
                        <p align="justify" style="width:1150px">Please read Privacy Policy
                        </p>
                        
                        <h4>Reservation of Rights</h4>
                        <p align="justify" style="width:1150px">We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.
                        </p>
                      
                        <h4>Removal of links from our website</h4>
                        <p align="justify" style="width:1150px">If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.
                        We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>

                        <h4>Disclaimer</h4>
                         <p align="justify" style="width:1150px">To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:
                        •   limit or exclude our or your liability for death or personal injury;
                        •   limit or exclude our or your liability for fraud or fraudulent misrepresentation;
                        •   limit any of our or your liabilities in any way that is not permitted under applicable law; or
                        •   exclude any of our or your liabilities that may not be excluded under applicable law.
                        The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.
                        As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.

                        </p>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of about area  ====================-->
    
    <!--====================  newsletter area ====================-->
    <div class="newsletter-area newsletter-area-bg bg-img" data-bg="assets/img/backgrounds/newsletter-bg.jpg">
        <div class="newsletter-wrapper background-color--default-overlay space__inner--y60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-10 mx-auto">
                        <div class="row align-items-center">
                            <div class="col-lg-4 mb-3 mb-lg-0">
                                <!-- newsletter title -->
                                <h3 class="newsletter-title"><span>Newsletter</span> Subscribe now</h3>
                            </div>
                            <div class="col-lg-8">
                                <div class="newsletter-form">
                                    <form id="mc-form" class="mc-form">
                                        <input type="email" placeholder="To get update, enter your email">
                                        <button class="theme-button" type="submit">Subscribe</button>
                                    </form>
                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of newsletter area  ====================-->
    <!--====================  footer area ====================-->
    <?php
        require_once('footer.php');
    ?>