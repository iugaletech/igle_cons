  <!--====================  footer area ====================-->
 <div class="footer-area bg-img space__inner--ry120" data-bg="assets/img/backgrounds/footer-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <div class="footer-widget">
                        <div class="footer-widget__logo space__bottom--40">
                            <a href="index">
                                <img src="assets/img/logo-white.png" class="img-fluid" alt="">
                            </a>
                        </div>
                        <p class="footer-widget__text space__bottom--20">Our Company offers the ultimate destination for building construction and renovation services for all sorts of properties.
                        Structures for a good fortune and Amazing revolution.</p>
                        <ul class="social-icons">
                            <li><a href="//www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="//www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="//www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="//plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">
                        <h5 class="footer-widget__title space__bottom--20">Important Links</h5>
                        <ul class="footer-widget__menu">
                            <li><a href="about">About us</a></li>
                            <li><a href="services">Services</a></li>
                            <li><a href="projects">Projects</a></li>
                            <li><a href="contact">Contact us</a></li>
                            <li><a href="privacy-policy">Privacy policy</a></li>
                            <li><a href="terms-and-conditions">Terms & Conditions</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h5 class="footer-widget__title space__top--15 space__bottom--20 space__top__md--40 space__top__lm--40">Contact us</h5>
                    <div class="footer-contact-wrapper">
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-map-marker"></i></div>
                            <div class="single-footer-contact__text">No-5 1st Cross Rd,<br>Pillanna Garden Stage III,<br>Kadugondanahalli, Bengaluru,<br>Karnataka - 560045</div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-phone"></i></div>
                            <div class="single-footer-contact__text"> <a href="tel:9380737109">9380737109</a> <br> <a href="tel:9008827201">9008827201</a> <br> <a href="tel:7019782560">7019782560</a> </div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-globe"></i></div>
                            <div class="single-footer-contact__text"><a href="mailto:contact@iugaleconstructions.com">contact@iugaleconstructions.com</a> <br> <a href="#"></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright text -->
    <div class="copyright-area background-color--deep-dark space__inner--y30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright-text">Copyright &copy; <a href="#"></a>, All Rights Reserved - 2020</p>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of footer area  ====================-->
    <!--====================  scroll top ====================-->
    <button class="scroll-top" id="scroll-top">
        <i class="fa fa-angle-up"></i>
    </button>
    <!--====================  End of scroll top  ====================-->
    <!-- JS
    ============================================ -->
    <!-- Modernizer JS -->
    <script src="assets/js/modernizr-2.8.3.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Slick slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- Counterup JS -->
    <script src="assets/js/plugins/counterup.min.js"></script>
    <!-- Waypoint JS -->
    <script src="assets/js/plugins/waypoint.min.js"></script>
    <!-- Justified Gallery JS -->
    <script src="assets/js/plugins/justifiedGallery.min.js"></script>
    <!-- Image Loaded JS -->
    <script src="assets/js/plugins/imageloaded.min.js"></script>
    <!-- Maosnry JS -->
    <script src="assets/js/plugins/masonry.min.js"></script>
    <!-- Light Gallery JS -->
    <script src="assets/js/plugins/light-gallery.min.js"></script>
    <!-- Mailchimp JS -->
    <script src="assets/js/plugins/mailchimp-ajax-submit.min.js"></script>
    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->
    <!--
    <script src="assets/js/plugins/plugins.min.js"></script>
    -->
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>